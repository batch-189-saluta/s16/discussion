let firstNumber = 12;
let secondNumber = 5;
let total = 0;


// ARITHMETIC OPERATORS
// addition operator - adds two or more numbers
total = firstNumber + secondNumber;
console.log('Result of addition operator is ' + total);

// subtraction operator - subtract two or more numbers
total = firstNumber - secondNumber;
console.log('Result of subtraction operator is ' + total);

// multiplication operator - multiply two or more numbers
total = firstNumber * secondNumber;
console.log('Result of multiplication operator is ' + total);

// division operator - divide two or more numbers
total = firstNumber / secondNumber;
console.log('Result of division operator is ' + total);

// modulo operator - remainder ng division
total = firstNumber % secondNumber;
console.log('Result of modulo operator is ' + total)


// ASSIGNMENT OPERATORS
// Reassignment Operator - should be a single equal sign, signifies re-assignment or new value into existing variable
total = 27;
console.log('Result of reassignment operator is ' + total)

/*addition assignment operator - uses the current value of the
	variable and ADDS a number to itself. afterwards, reassign 
	as the new value*/ 
// total = total + 3
total += 3;
console.log('Result of addition assignment operator is ' + total)

/*subtraction assignment operator - uses the current value of the
	variable and SUBTRACTS a number to itself. afterwards, reassign 
	as the new value*/ 
// total = total - 5
total -= 5;
console.log('Result of subtraction assignment operator is ' + total)


/*multiplication assignment operator - uses the current value of the
	variable and MULTIPLIES a number to itself. afterwards, reassign 
	as the new value*/ 
// total = total * 4
total *= 4;
console.log('Result of multiplication assignment operator is ' + total)

/*division assignment operator - uses the current value of the
	variable and DIVIDES a number to itself. afterwards, reassign 
	as the new value*/ 
// total = total / 20
total /= 20;
console.log('Result of division assignment operator is ' + total)


//MULTIPLE OPERATORS
let mdastotal = 0;
let pemdastotal = 0;

/*
when doing multiple operations, the program follows the MDAS rule
*/
mdastotal = 2 + 1 - 5 * 4 / 1; 
console.log(mdastotal)


/*
when doing multiple operations, the program follows the PEMDAS rule
	() = parenthesis , ** = exponent
*/
pemdastotal = 5**2 + (10-2) / 2 * 3;
console.log(pemdastotal);


// INCREMENT AND DECREMENT OPERATORS
let incrementNumber = 1;
let decrementNumber = 5;

/*
	pre-increment - adds 1 first before reading the value
	pre-decrement - subtract 1 first before reading the value
	post-increment - read the value first before adding 1
	post-decrement - reads the value first before subtracting 1
*/
let resultOfPreIncrement = ++incrementNumber
let resultOfPreDecrement = --decrementNumber
let	resultOfPostIncrement = incrementNumber++;
let resultOfPostDecrement = decrementNumber--

console.log(resultOfPreIncrement);
console.log(resultOfPreDecrement);
console.log(resultOfPostIncrement);
console.log(resultOfPostDecrement);


// COERCION
// Coercion - happens when you add string to non-string
let a = '10'
let b = 10
console.log(a + b);

// non-coercion
let d = 10
let f = 10
console.log(d + f);

// typeof keyword - returns the data type of a variable
let stringType = 'Hello'
let numberType = 1;
let booleanType = true;
let arrayType = ['1', '2', '3']
let objectType = {
	objectKey: 'Object Value',
}

console.log(typeof stringType)
console.log(typeof numberType)
console.log(typeof booleanType)
console.log(typeof arrayType)
console.log(typeof objectType)


// computer reads 'true' as 1
// computer reads 'false' as 0
console.log(true + 1)
console.log(false + 1)


// COMPARISON OPERATORS
/*equality operator - checks if both values are the same, returns
						true if it is
*/ 

console.log(5 == 5);
console.log('Hello' == 'Hello')
// hindi strict sa data type comparison pag '=='
console.log(2 == '2')

/*strict equality operator - checks if both values are the same, returns
						false if it is
*/ 
// strict sa data type comparison pag '==='
console.log(2 === '2')
console.log(true === 1)

/*Inequality operator - checks if both values are not equal
returns true if they aren't
*/
console.log(5 != 5);
console.log('Hello' != 'Hello')
console.log(2 != '2')

/*Strict Inequality operator - checks if both values AND data are not equal
returns true if they aren't.
*/
console.log(1 !== '1')


// RELATIONAL OPERATORS
let firstVariable = 5;
let secondVariable = 5;

// Greater than operator check if value is greater than the second
console.log(firstVariable > secondVariable);
// Less than operator check if value is lesser than the second
console.log(firstVariable < secondVariable);
// Greater than or equal to operator check if value is greater than OR equal to the second
console.log(firstVariable >= secondVariable);
// Lesser than or equal to operator check if value is lesser than OR equal to the second
console.log(firstVariable <= secondVariable);

// LOGICAL OPERATORS
let isLegalAge = true;
let isRegistered = false;

// AND operator - chinecheck if BOTH values are the same. if yes, true
console.log(isLegalAge && isRegistered);

// OR Operator - kahit yung isa lang ang true, true lalabas
console.log(isLegalAge || isRegistered);

// NOT operator - nirereverse yung value ng boolean (e.g. true to false)
console.log(!isLegalAge)


// Truthy and Falsey values - 
// Everything that is either empty, zero, or null will equate to false
// Everything that has some sort of value will equate to true
console.log([] == false);
console.log('' == false);
console.log(0 == false);
// console.log(null == false);
  

